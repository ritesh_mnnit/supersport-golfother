﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Globalization;

namespace golfother
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public SqlConnection dbConn;
        bool dbConnected = true;
        bool useProxy = Convert.ToBoolean(ConfigurationManager.AppSettings["useProxy"]);
        public DateTime statsRun = DateTime.Now.AddDays(-1);
        runInfo curRun;
        public string matchplaySlugname = ConfigurationManager.AppSettings["tournamentUrl"];

        internal void TestStartupAndStop()
        {
            this.OnStart(null);
            Console.ReadLine();
            this.OnStop();
        }

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected && dbConn != null)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        dbConnected = false;
                    }
                }
            }
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        private void run()
        {
            curRun = new runInfo();
            curRun.Start = DateTime.Now;
            curRun.Errors = new List<string>();

            try
            {
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
            }
            catch (Exception ex)
            {
                dbConnected = false;
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }

            if (dbConnected && dbConn != null)
            {
                try
                {
                    dbConn.StateChange += new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }

                try
                {
                    dbConn.StateChange -= new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }
            }
            else
            {
                curRun.Errors.Add("Failed to connect to the database");
            }

            TeamTalk(false);

            curRun.End = DateTime.Now;
            endRun();
            writeInfo();

            if (dbConn != null)
            {
                dbConn.Dispose();
            }

            sysTimer = new System.Timers.Timer(60000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        private WebProxy returnProxy()
        {
            WebProxy tmpProxy = new WebProxy();
            FileStream Fs = new FileStream("C:\\SuperSport\\proxyDetails.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader Sw = new StreamReader(Fs);
            Sw.BaseStream.Seek(0, SeekOrigin.Begin);
            string proxyUsername = Sw.ReadLine();
            string proxyPassword = Sw.ReadLine();
            string proxyDomain = Sw.ReadLine();
            string proxyAddress = Sw.ReadLine();
            int proxyPort = Convert.ToInt32(Sw.ReadLine());
            Sw.Close();
            Fs.Close();
            Fs.Dispose();

            System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
            tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
            tmpProxy.Credentials = ProxyCredentials;

            return tmpProxy;
        }

        private void writeInfo()
        {
            try
            {
                FileStream Fs = new FileStream(ConfigurationManager.AppSettings["log"], FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                SqlCommand SqlQuery = new SqlCommand("Insert Into golf.dbo.leaderboardIngest (tournament, startTime, endTime, errors, timeStamp) Values (100, @startTime, @endTime, @errors, @timeStamp)", dbConn);
                SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private bool isOtherTournament(string tournament)
        {
            string tournamentId = ConfigurationManager.AppSettings["tournamentId"];

            if (tournamentId == tournament)
                return true;

            return false;
        }

        private void TeamTalk(bool test)
        {
            try
            {
                string[] files;
                FileInfo fi;

                if (Directory.Exists("C:/inetpub/ftproot/teamtalk/golf"))
                {
                    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/teamtalk/golf"))
                    {
                        fi = new FileInfo(file);
                        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                        {
                            if (fi.Name.IndexOf("livescores") >= 0)
                            {
                                fi.CopyTo("c:/SuperSport/Golf/Other/Xml/TeamTalk/Temp/" + fi.Name);
                            }
                            fi.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message.ToString());
            }

            if (test)
            {
                //dbConn = new SqlConnection("Server=BPZ0NW1;UID=AllAccess;PWD=123456;DATABASE=golf;TrustServerCertificate=True");
                //dbConn = new SqlConnection("Server=10.16.208.128,55540;UID=tlns;PWD=pr0v1dence;DATABASE=SuperSportZone");
                dbConn = new SqlConnection("Server=10.10.4.40;UID=supersms;PWD=5m5RrR4;DATABASE=golf");
                //dbConn = new SqlConnection("Server=197.80.203.29;UID=GolfStage;PWD=Ms4DbFgsj3k;DATABASE=golf");
                try
                {
                    dbConn.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }

            if (Directory.Exists("c:/SuperSport/Golf/Other/Xml/TeamTalk/Temp/"))
            {
                foreach (string tmpFile in Directory.GetFiles("c:/SuperSport/Golf/Other/Xml/TeamTalk/Temp/"))
                {
                    FileInfo Fi = new FileInfo(tmpFile);
                    try
                    {
                        bool Done = false;

                        if (Fi.Name.ToLower().IndexOf(".xml") >= 0 && Fi.Length > 0)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load("c:/SuperSport/Golf/Other/Xml/TeamTalk/Temp/" + Fi.Name.ToLower());

                            int matchplayFinalMatchNumber = 201;
                            SqlCommand cmd = new SqlCommand("SELECT Content FROM [SuperSportZone].[dbo].[zonesitecontent] where Type = 'Golf Matchplay - Final Matchnumber'", dbConn);

                            int.TryParse(cmd.ExecuteScalar().ToString(), out matchplayFinalMatchNumber);

                            int matchplayThirdPlaceMatchNumber = 200;
                            cmd = new SqlCommand("SELECT Content FROM [SuperSportZone].[dbo].[zonesitecontent] where Type = 'Golf Matchplay - 3rd Place Matchnumber'", dbConn);

                            int.TryParse(cmd.ExecuteScalar().ToString(), out matchplayThirdPlaceMatchNumber);

                            foreach (XmlNode eventNode in xmlDoc.SelectNodes("livescores/event"))
                            {
                                DateTime timestamp = DateTime.Now;
                                foreach (XmlAttribute attribute in eventNode.Attributes)
                                {
                                    string attributeName = attribute.Name;
                                    switch (attributeName)
                                    {
                                        case "updatetimestamp":
                                            timestamp = DateTime.ParseExact(attribute.InnerText, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                foreach (XmlNode tournamentNode in eventNode.SelectNodes("tournament"))
                                {
                                    string status = string.Empty;
                                    int round = 1;
                                    string tournId = string.Empty;
                                    string name = string.Empty;
                                    string shortName = string.Empty;
                                    string location = string.Empty;
                                    string country = string.Empty;
                                    int tournament = Convert.ToInt32(ConfigurationManager.AppSettings["tournament"]);
                                    string tour = ConfigurationManager.AppSettings["tour"];
                                    string type = ConfigurationManager.AppSettings["matchType"];
                                    string tournamentUrl = ConfigurationManager.AppSettings["tournamentUrl"];
                                    bool insert = false;
                                    bool isMatchPlay = false;

                                    foreach (XmlAttribute attribute in tournamentNode.Attributes)
                                    {
                                        string attributeName = attribute.Name;
                                        switch (attributeName)
                                        {
                                            case "tournid":
                                                tournId = attribute.InnerText;
                                                break;
                                            case "name":
                                                name = attribute.InnerText;
                                                shortName = name;
                                                break;
                                            case "location":
                                                location = attribute.InnerText;
                                                break;
                                            case "country":
                                                country = attribute.InnerText;
                                                break;
                                            case "currentround":
                                                bool result = Int32.TryParse(attribute.InnerText, out round);
                                                break;
                                            case "status":
                                                status = attribute.InnerText;
                                                status = FixStatus(status);
                                                break;
                                            case "tournamentstatus":
                                                status = attribute.InnerText;
                                                status = FixStatus(status);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    if (isOtherTournament(tournId))
                                    {
                                        insert = true;
                                        if (tournId == "816")
                                        {
                                            round += 2;
                                        }
                                    }

                                    SqlCommand sqlQuery;

                                     if (insert)
                                    {
                                        if (tournament > 0)
                                        {
                                            sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard_Details SET tournId = @tournid, name = @name, shortName = @shortName, location = @location, country = @country, currentRound = @round, status =  @status WHERE (Id = @id)", dbConn);
                                            sqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = tournament;
                                            sqlQuery.Parameters.Add("@Round", SqlDbType.Int).Value = round;
                                            sqlQuery.Parameters.Add("@tournid", SqlDbType.VarChar).Value = tournId;
                                            sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                                            sqlQuery.Parameters.Add("@shortName", SqlDbType.VarChar).Value = shortName;
                                            sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                                            sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                                            sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                                            sqlQuery.ExecuteNonQuery();
                                        }

                                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                        int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                                        if (leaderboardPresent > 0)
                                        {
                                            sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                                        }
                                        else
                                        {
                                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                                        }
                                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                        sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                                        sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = shortName;
                                        sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                                        sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                                        sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                                        sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = 0;
                                        sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                                        sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                                        sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = timestamp;
                                        sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                                        sqlQuery.ExecuteNonQuery();

                                        if (!String.IsNullOrEmpty(tournamentUrl))
                                        {
                                            sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tournamentUrl;
                                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                            leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                                            if (leaderboardPresent > 0)
                                            {
                                                sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                                            }
                                            else
                                            {
                                                sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                                            }
                                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tournamentUrl;
                                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                            sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                                            sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = shortName;
                                            sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                                            sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                                            sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                                            sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = 0;
                                            sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                                            sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                                            sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = timestamp;
                                            sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                                            sqlQuery.ExecuteNonQuery();
                                        }
                                    }

                                    if (tournId == ConfigurationManager.AppSettings["MatchPlayTournament"])
                                    {
                                        DoTeamMatchPlayProcessing(tournamentNode, tournId);
                                    }
                                    else
                                    {
                                        XmlNodeList xmlNodeCount = tournamentNode.SelectNodes("players/player");
                                        if (xmlNodeCount.Count > 0)
                                        {
                                            int sorting = 1;
                                            StringBuilder sb = new StringBuilder();
                                            StringBuilder sb1 = new StringBuilder();
                                            StringBuilder sb2 = new StringBuilder();
                                            sb.Append("DELETE FROM golf.dbo.leaderboard_scores WHERE (tournament = " + tournament + ");");
                                            sb1.Append("DELETE FROM golf.dbo.leaderboardScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");");
                                            if (!String.IsNullOrEmpty(tournamentUrl))
                                            {
                                                sb2.Append("DELETE FROM golf.dbo.leaderboardScores WHERE (tour = '" + tournamentUrl + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");");
                                            }

                                            foreach (XmlNode playerNode in tournamentNode.SelectNodes("players/player"))
                                            {
                                                int pos = 1000;
                                                int tied = 0;
                                                string firstname = string.Empty;
                                                string lastname = string.Empty;
                                                string playerCountry = string.Empty;
                                                string playerId = string.Empty;
                                                int holesPlayed = 0;
                                                string round1ToPar = string.Empty;
                                                string round2ToPar = string.Empty;
                                                string round3ToPar = string.Empty;
                                                string round4ToPar = string.Empty;
                                                string round5ToPar = string.Empty;
                                                string round6ToPar = string.Empty;
                                                string round1Strokes = string.Empty;
                                                string round2Strokes = string.Empty;
                                                string round3Strokes = string.Empty;
                                                string round4Strokes = string.Empty;
                                                string round5Strokes = string.Empty;
                                                string round6Strokes = string.Empty;
                                                string total = string.Empty;
                                                string playerStatus = string.Empty;
                                                DateTime teeTime = new DateTime(1900, 1, 1, 0, 0, 0);

                                                foreach (XmlAttribute attribute in playerNode.Attributes)
                                                {
                                                    string attributeName = attribute.Name;
                                                    switch (attributeName)
                                                    {
                                                        case "firstname":
                                                            firstname = attribute.InnerText;
                                                            break;
                                                        case "lastname":
                                                            lastname = attribute.InnerText;
                                                            break;
                                                        case "country":
                                                            playerCountry = attribute.InnerText;
                                                            if (playerCountry == "ZAF")
                                                            {
                                                                playerCountry = "RSA";
                                                            }
                                                            break;
                                                        case "id":
                                                            playerId = attribute.InnerText;
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }

                                                foreach (XmlNode detailNode in playerNode.ChildNodes)
                                                {
                                                    string nodeName = detailNode.Name;
                                                    switch (nodeName)
                                                    {
                                                        case "totals":
                                                            foreach (XmlAttribute attribute in detailNode.Attributes)
                                                            {
                                                                string attributeName = attribute.Name;
                                                                switch (attributeName)
                                                                {
                                                                    case "totaltopar":
                                                                        total = attribute.InnerText;
                                                                        total = fixToPar(total);
                                                                        break;
                                                                    case "position":
                                                                        bool result = Int32.TryParse(attribute.InnerText, out pos);
                                                                        if (pos == 0)
                                                                        {
                                                                            pos = 1000;
                                                                        }
                                                                        break;
                                                                    case "tied":
                                                                        tied = attribute.InnerText.IndexOf("1") >= 0 ? 1 : 0;
                                                                        break;
                                                                    case "status":
                                                                        playerStatus = attribute.InnerText;
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                            }
                                                            break;
                                                        case "round":
                                                            int curRound = 1;
                                                            foreach (XmlAttribute attribute in detailNode.Attributes)
                                                            {
                                                                string attributeName = attribute.Name;
                                                                switch (attributeName)
                                                                {
                                                                    case "no":
                                                                        bool result1 = Int32.TryParse(attribute.InnerText, out curRound);
                                                                        break;
                                                                    case "strokes":
                                                                        switch (curRound)
                                                                        {
                                                                            case 1:
                                                                                round1Strokes = attribute.InnerText;
                                                                                break;
                                                                            case 2:
                                                                                round2Strokes = attribute.InnerText;
                                                                                break;
                                                                            case 3:
                                                                                round3Strokes = attribute.InnerText;
                                                                                break;
                                                                            case 4:
                                                                                round4Strokes = attribute.InnerText;
                                                                                break;
                                                                            case 5:
                                                                                round5Strokes = attribute.InnerText;
                                                                                break;
                                                                            case 6:
                                                                                round6Strokes = attribute.InnerText;
                                                                                break;
                                                                            default:
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "totaltopar":
                                                                        switch (curRound)
                                                                        {
                                                                            case 1:
                                                                                round1ToPar = attribute.InnerText;
                                                                                round1ToPar = fixToPar(round1ToPar);
                                                                                break;
                                                                            case 2:
                                                                                round2ToPar = attribute.InnerText;
                                                                                round2ToPar = fixToPar(round2ToPar);
                                                                                break;
                                                                            case 3:
                                                                                round3ToPar = attribute.InnerText;
                                                                                round3ToPar = fixToPar(round3ToPar);
                                                                                break;
                                                                            case 4:
                                                                                round4ToPar = attribute.InnerText;
                                                                                round4ToPar = fixToPar(round4ToPar);
                                                                                break;
                                                                            case 5:
                                                                                round5ToPar = attribute.InnerText;
                                                                                round5ToPar = fixToPar(round5ToPar);
                                                                                break;
                                                                            case 6:
                                                                                round6ToPar = attribute.InnerText;
                                                                                round6ToPar = fixToPar(round6ToPar);
                                                                                break;
                                                                            default:
                                                                                break;
                                                                        }
                                                                        break;
                                                                    case "holesplayed":
                                                                        if (curRound == round)
                                                                        {
                                                                            bool result2 = Int32.TryParse(attribute.InnerText, out holesPlayed);
                                                                        }
                                                                        break;
                                                                    case "teetime":
                                                                        if (attribute.InnerText.IndexOf(":") >= 0)
                                                                        {
                                                                            string[] time = attribute.InnerText.Split(':');
                                                                            teeTime = new DateTime(1900, 1, 1, Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), 0);
                                                                            teeTime = teeTime.AddHours(2);
                                                                        }
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                            }
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }

                                                if (playerStatus == "ok")
                                                {
                                                    sb.Append("INSERT INTO golf.dbo.leaderboard_scores (Tournament, Pos, Sorting, FirstName, lastName, Country, HolesPlayed, Round1ToPar, Round2ToPar, Round3ToPar, Round4ToPar, Round5ToPar, Round6ToPar, Round1Strokes, Round2Strokes, Round3Strokes, Round4Strokes, Round5Strokes, Round6Strokes, Total, TeeTime, Tied) VALUES (" + tournament + ", " + pos + ", " + sorting + ", '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', " + holesPlayed + ", '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round5ToPar + "', '" + round6ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + round5Strokes + "', '" + round6Strokes + "', '" + total + "', '" + teeTime + "', " + tied + ");");
                                                    sb1.Append("INSERT INTO golf.dbo.leaderboardScores (tour, tournament, year, position, sorting, tied, playerId, firstName, lastName, country, round1topar, round2topar, round3topar, round4topar, round5topar, round6topar, round1strokes, round2strokes, round3strokes, round4strokes, round5strokes, round6strokes, total, holesPlayed, teeTime) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + pos + "', " + sorting + ", " + tied + ", " + playerId + ", '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round5ToPar + "', '" + round6ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + round5Strokes + "', '" + round6Strokes + "', '" + total + "', " + holesPlayed + ", '" + teeTime + "');");
                                                    if (!String.IsNullOrEmpty(tournamentUrl))
                                                    {
                                                        sb2.Append("INSERT INTO golf.dbo.leaderboardScores (tour, tournament, year, position, sorting, tied, playerId, firstName, lastName, country, round1topar, round2topar, round3topar, round4topar, round5topar, round6topar, round1strokes, round2strokes, round3strokes, round4strokes, round5strokes, round6strokes, total, holesPlayed, teeTime) VALUES ('" + tournamentUrl.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + pos + "', " + sorting + ", " + tied + ", " + playerId + ", '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round5ToPar + "', '" + round6ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + round5Strokes + "', '" + round6Strokes + "', '" + total + "', " + holesPlayed + ", '" + teeTime + "');");
                                                    }
                                                    sorting += 1;
                                                }
                                            }

                                            if (insert && !String.IsNullOrEmpty(sb.ToString()))
                                            {
                                                sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }
                                            if (insert && !String.IsNullOrEmpty(sb1.ToString()))
                                            {
                                                sqlQuery = new SqlCommand(sb1.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }
                                            if (insert && !String.IsNullOrEmpty(sb2.ToString()))
                                            {
                                                sqlQuery = new SqlCommand(sb2.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }

                                            sb = new StringBuilder();
                                            sb1 = new StringBuilder();
                                            sb2 = new StringBuilder();
                                            sqlQuery = new SqlCommand("SELECT * FROM golf.dbo.leaderboard_scores WHERE (Tournament = " + tournament + ") ORDER BY Pos, teeTime", dbConn);
                                            SqlDataReader RsRec = sqlQuery.ExecuteReader();
                                            sorting = 1;
                                            while (RsRec.Read())
                                            {
                                                sb.Append("UPDATE golf.dbo.leaderboard_scores Set sorting = " + sorting + " WHERE (tournament = " + tournament + " AND Pos = " + RsRec["Pos"].ToString() + " AND Lastname = '" + RsRec["Lastname"].ToString().Replace("'", "''") + "')");
                                                sb1.Append("UPDATE golf.dbo.leaderboardScores Set sorting = " + sorting + " WHERE (tour = '" + tour + "' AND tournament = " + tournId + " AND Year = " + DateTime.Now.Year + " AND position = " + RsRec["Pos"].ToString() + " AND Lastname = '" + RsRec["Lastname"].ToString().Replace("'", "''") + "')");
                                                if (!String.IsNullOrEmpty(tournamentUrl))
                                                {
                                                    sb2.Append("UPDATE golf.dbo.leaderboardScores Set sorting = " + sorting + " WHERE (tour = '" + tournamentUrl + "' AND tournament = " + tournId + " AND Year = " + DateTime.Now.Year + " AND position = " + RsRec["Pos"].ToString() + " AND Lastname = '" + RsRec["Lastname"].ToString().Replace("'", "''") + "')");
                                                }
                                                sorting += 1;
                                            }
                                            RsRec.Close();
                                            if (insert && !String.IsNullOrEmpty(sb.ToString()))
                                            {
                                                sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }
                                            if (insert && !String.IsNullOrEmpty(sb1.ToString()))
                                            {
                                                sqlQuery = new SqlCommand(sb1.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }
                                            if (insert && !String.IsNullOrEmpty(sb2.ToString()))
                                            {
                                                sqlQuery = new SqlCommand(sb2.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }
                                        }

                                        XmlNodeList xm1 = tournamentNode.SelectNodes("bracket");
                                        XmlNodeList xm2 = tournamentNode.SelectNodes("interbracket");
                                        XmlNodeList xm3 = tournamentNode.SelectNodes("playoffs");

                                        var joinedxmlNodeCount = xm1.Cast<XmlNode>().Concat(xm2.Cast<XmlNode>()).Concat(xm3.Cast<XmlNode>()).ToList();

                                        if (joinedxmlNodeCount.Count <= 0)
                                        {
                                            xmlNodeCount = tournamentNode.SelectNodes("round");
                                            joinedxmlNodeCount = xmlNodeCount.Cast<XmlNode>().ToList();
                                        }
                                        if (joinedxmlNodeCount.Count > 0)
                                        {
                                            int sorting = 1;
                                            int bracket = 0;
                                            StringBuilder sb = new StringBuilder();
                                            sb.Append("DELETE FROM golf.dbo.leaderboardMatchPlayScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");");

                                            foreach (XmlNode bracketNode in joinedxmlNodeCount)
                                            {
                                                string bracketName = String.Empty;
                                                sorting = 1;

                                                foreach (XmlAttribute attribute in bracketNode.Attributes)
                                                {
                                                    string attributeName = attribute.Name;
                                                    switch (attributeName)
                                                    {
                                                        case "name":
                                                            bracketName = attribute.Value;
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }

                                                if (!String.IsNullOrEmpty(bracketName) || bracketNode.Name.ToLower() == "interbracket" || bracketNode.Name.ToLower() == "playoffs")
                                                {
                                                    bracket += 1;
                                                }

                                                xmlNodeCount = bracketNode.SelectNodes("matches");
                                                XmlNodeList xmlMatches;
                                                if (xmlNodeCount.Count > 0)
                                                {
                                                    xmlMatches = bracketNode.SelectNodes("matches/matchinfo");
                                                }
                                                else
                                                {
                                                    xmlMatches = bracketNode.SelectNodes("matchinfo");
                                                }

                                                foreach (XmlNode xmlMatch in xmlMatches)
                                                {
                                                    int matchNumber = 0;
                                                    int holesPlayed = 0;
                                                    int curRound = 1;
                                                    string roundName = string.Empty;
                                                    DateTime teeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                                                    string course = String.Empty;
                                                    string matchStatus = string.Empty;
                                                    string matchScore = string.Empty;
                                                    string matchWinnerId = string.Empty;

                                                    int p1PreviousMatch = 0;
                                                    string p1Id = string.Empty;
                                                    int p1Seed = 0;
                                                    string p1FirstName = string.Empty;
                                                    string p1LastName = string.Empty;
                                                    string p1Country = string.Empty;
                                                    int p1Winner = 0;
                                                    string p1curStatus = string.Empty;

                                                    int p2PreviousMatch = 0;
                                                    string p2Id = string.Empty;
                                                    int p2Seed = 0;
                                                    string p2FirstName = string.Empty;
                                                    string p2LastName = string.Empty;
                                                    string p2Country = string.Empty;
                                                    int p2Winner = 0;
                                                    string p2curStatus = string.Empty;

                                                    foreach (XmlAttribute attribute in xmlMatch.Attributes)
                                                    {
                                                        string attributeName = attribute.Name;
                                                        switch (attributeName)
                                                        {
                                                            case "holesplayed":
                                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                                {
                                                                    bool result = Int32.TryParse(attribute.InnerText.ToString(), out holesPlayed);
                                                                }
                                                                break;
                                                            case "matchstatus":
                                                                matchStatus = attribute.Value;
                                                                break;
                                                            case "score":
                                                                if (attribute.Value.ToLower() == "1 hole" || attribute.Value.ToLower() == "2 holes")
                                                                    matchScore = attribute.Value.Replace(" Hole", "").Replace("s", "") + "UP";
                                                                else
                                                                    matchScore = attribute.Value;
                                                                break;
                                                            case "winnerid":
                                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                                {
                                                                    matchWinnerId = attribute.InnerText.ToString();
                                                                }
                                                                break;
                                                            case "TeeDateTime":
                                                                teeTime = DateTime.ParseExact(attribute.InnerText.ToLower().Replace("oct", "Oct"), "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                                teeTime = teeTime.AddHours(2);
                                                                break;
                                                            case "teedatetime":
                                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                                {
                                                                    try
                                                                    {
                                                                        teeTime = DateTime.ParseExact(attribute.InnerText.ToLower().Replace("oct", "Oct"), "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                                        if (teeTime.TimeOfDay.Hours == 22)
                                                                            teeTime = teeTime.AddHours(1).AddMinutes(59);
                                                                        else
                                                                            teeTime = teeTime.AddHours(2);
                                                                    }
                                                                    catch (Exception exc)
                                                                    {
                                                                        try
                                                                        {
                                                                            teeTime = DateTime.ParseExact(attribute.InnerText.ToLower().Replace("oct", "Oct"), "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                                            if (teeTime.TimeOfDay.Hours == 22)
                                                                                teeTime = teeTime.AddHours(1).AddMinutes(59);
                                                                            else
                                                                                teeTime = teeTime.AddHours(2);
                                                                        }
                                                                        catch (Exception exx)
                                                                        {
                                                                            try
                                                                            {
                                                                                teeTime = DateTime.ParseExact(attribute.InnerText.ToLower().Replace("oct", "Oct"), "dd-MMM-yyyy H:mm", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                                                if (teeTime.TimeOfDay.Hours == 22)
                                                                                    teeTime = teeTime.AddHours(1).AddMinutes(59);
                                                                                else
                                                                                    teeTime = teeTime.AddHours(2);
                                                                            }
                                                                            catch (Exception exxx)
                                                                            {

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                break;
                                                            case "round":
                                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                                {
                                                                    bool result2 = Int32.TryParse(attribute.InnerText.ToString(), out curRound);
                                                                }
                                                                break;
                                                            case "matchnumber":
                                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                                {
                                                                    bool result3 = Int32.TryParse(attribute.InnerText.ToString(), out matchNumber);
                                                                }
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                    }

                                                    matchScore = GetScoreBeyond18Holes(matchStatus, holesPlayed, matchScore);

                                                    roundName = "Round " + curRound;

                                                    if (tournId == ConfigurationManager.AppSettings["tournamentId"])
                                                    {
                                                        /*teeTime = teeTime.AddHours(2);
                                                        if (curRound == 1)
                                                        {
                                                            if (teeTime.Day == 17)
                                                            {
                                                                if (teeTime.Hour > 8)
                                                                {
                                                                    curRound = 3;
                                                                    roundName = "Group stage, Round 3 – Friday";
                                                                }
                                                                else
                                                                {
                                                                    curRound = 2;
                                                                    roundName = "Group stage, Round 2 – Friday";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                roundName = "Group stage, Round 1 – Thursday";
                                                                curRound = 1;
                                                            }
                                                        }
                                                        else
                                                        {*/

                                                        switch (curRound)
                                                        {
                                                            case 4:
                                                                roundName = "Round of 16";
                                                                break;
                                                            case 5:
                                                                roundName = "Quarterfinals";
                                                                break;
                                                            case 6:
                                                                roundName = "Semi-finals";
                                                                break;
                                                            case 7:
                                                                roundName = "Finals";
                                                                break;
                                                            default:
                                                                roundName = "Round " + curRound;
                                                                break;
                                                        }
                                                    }

                                                    if(xmlMatches.Count == 4)
                                                    {
                                                        roundName = "Quarterfinals";
                                                    }

                                                    if(xmlMatches.Count == 2)
                                                    {
                                                        roundName = "Semi-finals";                                                       
                                                    }

                                                    if (matchNumber == matchplayThirdPlaceMatchNumber)
                                                    {
                                                        roundName = "3rd Place Playoff";
                                                    }
                                                    if (matchNumber == matchplayFinalMatchNumber)
                                                    {
                                                        curRound++;
                                                        roundName = "Final";
                                                    }

                                                    int player = 1;
                                                    foreach (XmlNode playerNode in xmlMatch.SelectNodes("player"))
                                                    {
                                                        if (player == 2)
                                                        {
                                                            foreach (XmlAttribute attribute in playerNode.Attributes)
                                                            {
                                                                string attributeName = attribute.Name;
                                                                switch (attributeName)
                                                                {
                                                                    case "country":
                                                                        p2Country = attribute.Value;
                                                                        break;
                                                                    case "score":
                                                                        p2curStatus = attribute.Value;
                                                                        if (p2curStatus.ToLower().IndexOf("down") >= 0 || p2curStatus.ToLower().IndexOf("dn") >= 0)
                                                                        {
                                                                            p2curStatus = "";
                                                                        }
                                                                        break;
                                                                    case "lastname":
                                                                        p2LastName = attribute.Value;
                                                                        break;
                                                                    case "firstname":
                                                                        p2FirstName = attribute.Value;
                                                                        break;
                                                                    case "id":
                                                                        p2Id = attribute.Value;
                                                                        if (p2Id == matchWinnerId)
                                                                        {
                                                                            p2Winner = 1;
                                                                        }
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (XmlAttribute attribute in playerNode.Attributes)
                                                            {
                                                                string attributeName = attribute.Name;
                                                                switch (attributeName)
                                                                {
                                                                    case "country":
                                                                        p1Country = attribute.Value;
                                                                        break;
                                                                    case "score":
                                                                        p1curStatus = attribute.Value;
                                                                        if (p1curStatus.ToLower().IndexOf("down") >= 0 || p1curStatus.ToLower().IndexOf("dn") >= 0)
                                                                        {
                                                                            p1curStatus = "";
                                                                        }
                                                                        break;
                                                                    case "lastname":
                                                                        p1LastName = attribute.Value;
                                                                        break;
                                                                    case "firstname":
                                                                        p1FirstName = attribute.Value;
                                                                        break;
                                                                    case "id":
                                                                        p1Id = attribute.Value;
                                                                        if (p1Id == matchWinnerId)
                                                                        {
                                                                            p1Winner = 1;
                                                                        }
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                            }
                                                            player += 1;
                                                        }
                                                    }

                                                    matchScore = MakeSureThereIsAlwaysAMatchScore(p1curStatus, p2curStatus, matchStatus, matchScore);

                                                    if (!String.IsNullOrEmpty(p1LastName))
                                                    {
                                                        sb.Append("INSERT INTO golf.dbo.leaderboardMatchPlayScores (tour, tournament, course, year, position, sorting, round, roundName, bracket, bracketName, matchNumber, teeTime, holesPlayed, P1PreviousMatch, P1Id, P1Seed, P1FirstName, P1LastName, P1Country, P1Winner, P1Status, P2PreviousMatch, P2Id, P2Seed, P2FirstName, P2LastName, P2Country, P2Winner, P2Status, status, matchStatus) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId + "', '" + course.Replace("'", "''") + "', " + DateTime.Now.Year + ", 0, " + sorting + ", " + curRound + ", '" + roundName + "', " + bracket + ", '" + bracketName.Replace("'", "''") + "', " + matchNumber + ", '" + teeTime.ToString("yyyy-MM-dd HH:mm") + "', " + holesPlayed + ", " + p1PreviousMatch + ", '" + p1Id.Replace("'", "''") + "', " + p1Seed + ", '" + p1FirstName.Replace("'", "''") + "', '" + p1LastName.Replace("'", "''") + "', '" + p1Country.Replace("'", "''") + "', " + p1Winner + ", '" + p1curStatus.Replace("'", "''") + "', " + p2PreviousMatch + ", '" + p2Id.Replace("'", "''") + "', " + p2Seed + ", '" + p2FirstName.Replace("'", "''") + "', '" + p2LastName.Replace("'", "''") + "', '" + p2Country.Replace("'", "''") + "', " + p2Winner + ", '" + p2curStatus.Replace("'", "''") + "', '" + matchScore.Replace("'", "''") +"', '" + matchStatus.Replace("'", "''") + "');");
                                                        sorting += 1;
                                                    }
                                                }
                                            }

                                            if (!String.IsNullOrEmpty(sb.ToString()) && sb.ToString().IndexOf("INSERT") >= 0)
                                            {
                                                sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                                                sqlQuery.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                }
                            }

                            xmlDoc = null;

                        }
                        Fi.CopyTo("c:/SuperSport/Golf/Other/Xml/TeamTalk/Processed/" + Fi.Name, true);
                        Fi.Delete();
                    }
                    catch (Exception ex)
                    {
                        if (!Directory.Exists("c:/SuperSport/Golf/Other/Xml/TeamTalk/Errors/"))
                        {
                            Directory.CreateDirectory("c:/SuperSport/Golf/Other/Xml/TeamTalk/Errors/");
                        }

                        Fi.CopyTo("c:/SuperSport/Golf/Other/Xml/TeamTalk/Errors/" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss_") + Fi.Name);
                        Fi.Delete();

                        curRun.Errors.Add(ex.Message.ToString());
                    }
                }
            }
             
            if (test)
            {
                dbConn.Close();
            }
        }

        private string MakeSureThereIsAlwaysAMatchScore(string p1curStatus, string p2curStatus, string matchStatus, string matchScore)
        {
            if (!String.IsNullOrEmpty(matchScore))
                return matchScore;

            if (matchStatus.ToLower().Contains("progress"))
            {
                if (String.IsNullOrEmpty(p1curStatus) && String.IsNullOrEmpty(p2curStatus))
                {
                    matchScore = "AS";
                }
                else
                {
                    if (String.IsNullOrEmpty(p1curStatus))
                    {
                        matchScore = p2curStatus;
                    }
                    else
                    {
                        matchScore = p1curStatus;
                    }
                }
            }

            if (!String.IsNullOrEmpty(p1curStatus) && !String.IsNullOrEmpty(p2curStatus))
            {
                matchScore = "AS";
            }

            return matchScore;
        }

        private string GetScoreBeyond18Holes(string matchStatus, int holesPlayed, string playerstatus)
        {
            if (matchStatus.ToLower() == "completed" && holesPlayed > 18)
            {
                return holesPlayed + "h";
            }

            return playerstatus;
        }

        private void DoTeamMatchPlayProcessing(XmlNode tournamentNode, string tournId)
        {
            XmlNodeList xmlNodeList = tournamentNode.SelectNodes("matches/match");

            string homeTeam = ConfigurationManager.AppSettings["MatchPlayHomeTeam"];
            string format = "";
            DateTime tournamentStartDate = Convert.ToDateTime(ConfigurationManager.AppSettings["MatchPlayTournamentStartDate"]);
            DateTime Day = new DateTime(1900, 1, 1, 0, 0, 0);
            int sorting = 0;
            int roundNo = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("DELETE FROM golf.dbo.leaderboardMatchPlayScores WHERE (tour='" + matchplaySlugname + "' AND year = " + DateTime.Now.Year + " AND tournament='" + tournId + "');");
                    
            foreach (XmlNode match in xmlNodeList)
            {
                string Team1Score = string.Empty;
                string Team2Score = string.Empty;
                bool isFirstTeam1Member = true;
                bool isFirstTeam2Member = true;

                int matchNumber = 0;
                int holesPlayed = 0;
                string roundName = string.Empty;
                DateTime teeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                string course = String.Empty;
                string matchStatus = string.Empty;
                string matchScore = string.Empty;
                string matchWinnerId = string.Empty;

                int p1PreviousMatch = 0;
                string p1Id = string.Empty;
                int p1Seed = 0;
                string p1FirstName = string.Empty;
                string p1LastName = string.Empty;
                string p1Country = string.Empty;
                int p1Winner = 0;
                string p1curStatus = string.Empty;

                int p2PreviousMatch = 0;
                string p2Id = string.Empty;
                int p2Seed = 0;
                string p2FirstName = string.Empty;
                string p2LastName = string.Empty;
                string p2Country = string.Empty;
                int p2Winner = 0;
                string p2curStatus = string.Empty;

                string p1_p2Id = string.Empty;
                int p1_p2Seed = 0;
                string p1_p2FirstName = string.Empty;
                string p1_p2LastName = string.Empty;
                string p1_p2Country = string.Empty;

                string p2_p2Id = string.Empty;
                int p2_p2Seed = 0;
                string p2_p2FirstName = string.Empty;
                string p2_p2LastName = string.Empty;
                string p2_p2Country = string.Empty;

                if (match.Attributes["TeeDateTime"] != null && !String.IsNullOrEmpty(match.Attributes["TeeDateTime"].InnerText))
                {
                    bool roundUpdated = false;

                    foreach (XmlAttribute attr in match.Attributes)
                    {
                        string attrString = attr.Name.ToLower();

                        switch (attrString)
                        {
                            case "teedatetime":
                                {
                                    if (!String.IsNullOrEmpty(attr.InnerText))
                                    {
                                        string temp = attr.InnerText.ToLower().Replace("oct", "Oct");

                                        if (temp.IndexOf(":00") < 0)
                                        {
                                            temp = temp + ":00";
                                        }

                                        teeTime = DateTime.ParseExact(temp, "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                        teeTime = teeTime.AddHours(2.0);

                                        if (Day.Day != teeTime.Day)
                                        {
                                            Day = teeTime;
                                            if (roundUpdated == false)
                                            {
                                                roundNo++;
                                                roundUpdated = true;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "format":
                                {
                                    string currentFormat = attr.InnerText;

                                    if (!format.Equals(currentFormat))
                                    {
                                        format = currentFormat;
                                        if (roundUpdated == false)
                                        {
                                            roundNo++;
                                            roundUpdated = true;
                                        }
                                    }
                                }
                                break;
                            case "matchnumber":
                                {
                                    bool temp = Int32.TryParse(attr.InnerText.ToString(), out matchNumber);
                                }
                                break;
                            case "matchstatus":
                                {
                                    matchStatus = attr.InnerText;
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    roundUpdated = false;

                    XmlNode score = match.SelectNodes("score") != null ? match.SelectNodes("score")[0] : null;

                    if (score != null)
                    {

                        foreach (XmlAttribute attr in score.Attributes)
                        {
                            switch (attr.Name.ToLower())
                            {
                                case "usascore":
                                    if (!String.IsNullOrEmpty(attr.InnerText) && attr.InnerText.ToLower().IndexOf("down") < 0)
                                    {
                                        if (homeTeam.Equals("usa"))
                                            p1curStatus = attr.InnerText;
                                        else
                                            p2curStatus = attr.InnerText;
                                    }
                                    break;

                                case "intscore":
                                    if (!String.IsNullOrEmpty(attr.InnerText) && attr.InnerText.ToLower().IndexOf("down") < 0)
                                    {
                                        if (homeTeam.Equals("int"))
                                            p1curStatus = attr.InnerText;
                                        else
                                            p2curStatus = attr.InnerText;
                                    }
                                    break;

                                case "":
                                    if (!String.IsNullOrEmpty(attr.InnerText) && attr.InnerText.ToLower().IndexOf("down") < 0)
                                    {
                                        if (homeTeam.Equals("int"))
                                            p1curStatus = attr.InnerText;
                                        else
                                            p2curStatus = attr.InnerText;
                                    }
                                    break;

                                case "holesplayed":
                                    bool temp = Int32.TryParse(attr.InnerText.ToString(), out holesPlayed);
                                    break;
                            }

                            //special case to use config value on the unknown team
                            if (attr.Name == ConfigurationManager.AppSettings["EuropeanScoreItemToCheck"])
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText) && attr.InnerText.ToLower().IndexOf("down") < 0)
                                {
                                    if (homeTeam.Equals(ConfigurationManager.AppSettings["MatchPlayHomeTeam"]))
                                        p2curStatus = attr.InnerText;                                    
                                    else
                                        p1curStatus = attr.InnerText;

                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(p1curStatus))
                        {
                            matchScore = p1curStatus;

                            if (matchStatus.ToLower() == "completed")
                            {
                                p1Winner = 1;
                            }
                        }

                        if (!String.IsNullOrEmpty(p2curStatus))
                        {
                            matchScore = p2curStatus;

                            if (matchStatus.ToLower() == "completed")
                            {
                                p2Winner = 1;
                            }
                        }

                        if (!String.IsNullOrEmpty(p1curStatus) && !String.IsNullOrEmpty(p2curStatus))
                        {
                            matchScore = p1curStatus;
                            p1Winner = 0;
                            p2Winner = 0;
                        }
                    }

                    XmlNodeList players = match.SelectNodes("player");

                    if (players != null && players.Count > 0)
                    {
                        foreach (XmlNode player in players)
                        {
                            string curPlayerId = string.Empty;
                            string curPlayerName = string.Empty;
                            string curPlayerLastName = string.Empty;
                            string curPlayerCountry = string.Empty;
                            string curPlayerTeam = string.Empty;

                            foreach (XmlAttribute attr in player.Attributes)
                            {
                                switch (attr.Name.ToLower())
                                {
                                    case "id":
                                        curPlayerId = attr.InnerText;
                                        break;
                                    case "firstname":
                                        curPlayerName = attr.InnerText;
                                        break;
                                    case "lastname":
                                        curPlayerLastName = attr.InnerText;
                                        break;
                                    case "country":
                                        curPlayerCountry = attr.InnerText;
                                        break;
                                    case "team":
                                        curPlayerTeam = attr.InnerText;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (homeTeam.Equals(curPlayerTeam.ToLower()))
                            {
                                if (isFirstTeam1Member)
                                {
                                    p1Id = curPlayerId;
                                    p1FirstName = curPlayerName;
                                    p1LastName = curPlayerLastName;
                                    p1Country = curPlayerCountry;

                                    isFirstTeam1Member = false;
                                }
                                else
                                {
                                    p1_p2Id = curPlayerId;
                                    p1_p2FirstName = curPlayerName;
                                    p1_p2LastName = curPlayerLastName;
                                    p1_p2Country = curPlayerCountry;
                                }
                            }
                            else
                            {
                                if (isFirstTeam2Member)
                                {
                                    p2Id = curPlayerId;
                                    p2FirstName = curPlayerName;
                                    p2LastName = curPlayerLastName;
                                    p2Country = curPlayerCountry;

                                    isFirstTeam2Member = false;
                                }
                                else
                                {
                                    p2_p2Id = curPlayerId;
                                    p2_p2FirstName = curPlayerName;
                                    p2_p2LastName = curPlayerLastName;
                                    p2_p2Country = curPlayerCountry;
                                }
                            }
                        }
                    }
                    sorting++;

                    roundName = "Day " + (teeTime.DayOfYear - tournamentStartDate.DayOfYear) + " - " + format;
                    sb.Append("INSERT INTO golf.dbo.leaderboardMatchPlayScores (tour,tournament,year,position,sorting,round,bracket,matchNumber,teeTime,holesPlayed,P1PreviousMatch,P1Id,P1Seed,P1FirstName,P1LastName,P1Country,P1Winner,P1Status,P2PreviousMatch,P2Id,P2Seed,P2FirstName,P2LastName,P2Country,P2Winner,P2Status,status,roundName,P1_P2Id,P1_P2Seed,P1_P2Firstname,P1_P2LastName,P1_P2Country,P2_P2Id,P2_P2Seed,P2_P2Firstname,P2_P2LastName,P2_P2Country, matchStatus) VALUES ('" + matchplaySlugname + "','" + tournId + "'," + DateTime.Now.Year + ",0," + sorting + "," + roundNo + "," + 0 + "," + matchNumber + ",'" + teeTime.ToString("yyyy-MM-dd HH:mm") + "'," + holesPlayed + "," + p1PreviousMatch + ",'" + p1Id.Replace("'", "''") + "'," + p1Seed + ",'" + p1FirstName.Replace("'", "''") + "','" + p1LastName.Replace("'", "''") + "','" + p1Country.Replace("'", "''") + "'," + p1Winner + ",'" + p1curStatus.Replace("'", "''") + "'," + p2PreviousMatch + ",'" + p2Id.Replace("'", "''") + "'," + p2Seed + ",'" + p2FirstName.Replace("'", "''") + "','" + p2LastName.Replace("'", "''") + "','" + p2Country.Replace("'", "''") + "'," + p2Winner + ",'" + p2curStatus.Replace("'", "''") + "','" + matchScore + "','" + roundName + "','" + p1_p2Id.Replace("'", "''") + "'," + p1_p2Seed + ",'" + p1_p2FirstName.Replace("'", "''") + "','" + p1_p2LastName.Replace("'", "''") + "','" + p1_p2Country.Replace("'", "''") + "','" + p2_p2Id.Replace("'", "''") + "'," + p2_p2Seed + ",'" + p2_p2FirstName.Replace("'", "''") + "','" + p2_p2LastName.Replace("'", "''") + "','" + p2_p2Country.Replace("'", "''") + "','" + matchStatus.Replace("'", "''") + "');");
                }
            }

            if (sb.ToString().IndexOf("INSERT") >= 0)
            {
                SqlCommand SqlQuery = new SqlCommand(sb.ToString(), dbConn);
                SqlQuery.ExecuteNonQuery();
            }

            XmlNode totalpoints = tournamentNode.SelectNodes("totalpoints") != null ? tournamentNode.SelectNodes("totalpoints")[0] : null;

            if (totalpoints != null)
            {
                CreateMatchPlayTournament(totalpoints, tournId, homeTeam);
            }

            CreateUpdateMatchPlayRounds(tournId);
        }

        private void CreateUpdateMatchPlayRounds(string tournId)
        {
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("DELETE FROM [Golf].[dbo].[leaderboardMatchPlayRounds] WHERE (year = " + DateTime.Now.Year + " AND tour = '" + matchplaySlugname + "' AND tournament = '" + tournId + "' );");
            ////sb1.Append("INSERT INTO [Golf].[dbo].[leaderboardMatchPlayRounds] (Tournament, Tour, Year, Number, Name, [Format], team1Score, team2Score) SELECT '" + tournId + "' As Tournament, 'golf-globe' As Tour, " + DateTime.Now.Year + " As Year, Round, RoundName, SUBSTRING(RoundName, 9, Len(Roundname) - 8) As Format, Sum(Case When p1Winner = 1 AND p2Winner = 1 Then 0.5 When p1Winner = 0 AND p2Winner = 0 Then 0.5 When p1Winner = 1 Then 1.0 Else 0.0 End) As Team1Points, Sum(Case When p1Winner = 1 AND p2Winner = 1 Then 0.5 When p1Winner = 0 AND p2Winner = 0 Then 0.5 When p2Winner = 1 Then 1.0 Else 0.0 End) As Team2Points FROM [Golf].[dbo].[leaderboardMatchPlayScores] WHERE (holesplayed = 18 Or (P1Winner = 1 Or p2Winner = 1)) AND tournament = '" + tournId + "' GROUP BY Round, RoundName ORDER By Round;");
            sb1.Append("INSERT INTO [Golf].[dbo].[leaderboardMatchPlayRounds] (Tournament, Tour, Year, Number, Name, [Format], team1Score, team2Score) SELECT '" + tournId + "' As Tournament, '" + matchplaySlugname + "' As Tour, " + DateTime.Now.Year + " As Year, Round, RoundName, SUBSTRING(RoundName, 9, Len(Roundname) - 8) As Format, Sum(Case When (p1Winner = 0 AND p2Winner = 0) And (HolesPlayed < 18) Then 0 When p1Winner = 1 AND p2Winner = 1 AND CHARINDEX('Single', Roundname) < 1 Then 0.5 When p1Winner = 0 AND p2Winner = 0 AND CHARINDEX('Single', Roundname) < 1 Then 0.5 When p1Winner = 1 Then 1.0 Else 0.0 End) As Team1Points, Sum(Case When (p1Winner = 0 AND p2Winner = 0) And (HolesPlayed < 18) Then 0 When p1Winner = 1 AND p2Winner = 1 AND CHARINDEX('Single', Roundname) < 1 Then 0.5 When p1Winner = 0 AND p2Winner = 0 AND CHARINDEX('Single', Roundname) < 1 Then 0.5 When p2Winner = 1 Then 1.0 Else 0.0 End) As Team2Points FROM [Golf].[dbo].[leaderboardMatchPlayScores] WHERE tournament = '" + tournId + "' GROUP BY Round, RoundName ORDER By Round;");
            new SqlCommand(sb1.ToString(), dbConn).ExecuteNonQuery();
        }

        private void CreateMatchPlayTournament(XmlNode totalpoints, string tournId, string homeTeam)
        {
            string Team1points = "0";
            string Team2points = "0";
            string Team1Name = string.Empty;
            string Team2Name = string.Empty;
            string Team1ShortName = string.Empty;
            string Team2ShortName = string.Empty;

            Team1Name = ConfigurationManager.AppSettings["MatchPlayHomeTeamLong"];
            Team1ShortName = ConfigurationManager.AppSettings["MatchPlayHomeTeamShort"];
            Team2Name = ConfigurationManager.AppSettings["MatchPlayAwayTeamLong"];
            Team2ShortName = ConfigurationManager.AppSettings["MatchPlayAwayTeamShort"];

            //if (homeTeam.Equals("int"))
            //{
            //    Team2Name = "United States";
            //    Team1Name = "Europe";
            //    Team2ShortName = "USA";
            //    Team1ShortName = "Eur";
            //}
            //else if (homeTeam.Equals("usa"))
            //{
            //    Team1Name = "United States";
            //    Team2Name = "Europe";
            //    Team1ShortName = "USA";
            //    Team2ShortName = "Eur";
            //}
            //else if (homeTeam.Equals(ConfigurationManager.AppSettings["MatchPlayHomeTeam"]))
            //{
            //    Team1Name = ConfigurationManager.AppSettings["MatchPlayHomeTeamLong"];
            //    Team2Name = "United States";
            //    Team1ShortName = ConfigurationManager.AppSettings["MatchPlayHomeTeam"].ToUpper();
            //    Team2ShortName = "USA";
            //}

            foreach (XmlAttribute attr in totalpoints.Attributes)
            {
                string team = attr.Name.ToLower();

                switch (team)
                {
                    case "unitedstatesteam":
                        {
                            if (homeTeam.Equals("int"))
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText))
                                {
                                    Team2points = attr.InnerText;
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText))
                                {
                                    Team1points = attr.InnerText;
                                }
                            }
                        }
                        break;
                    case "internationalteam":
                        {
                            if (homeTeam.Equals("int"))
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText))
                                {
                                    Team1points = attr.InnerText;
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText))
                                {
                                    Team2points = attr.InnerText;
                                }
                            }
                        }
                        break;
                    case "europeanteam":
                        {
                            if (homeTeam.Equals(ConfigurationManager.AppSettings["MatchPlayHomeTeam"]))
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText))
                                {
                                    Team2points = attr.InnerText;
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(attr.InnerText))
                                {
                                    Team1points = attr.InnerText;
                                }
                            }
                        }
                        break;
                }
            }

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("SELECT * FROM golf.dbo.leaderboardMatchPlay WHERE (year = " + DateTime.Now.Year + " AND tour = '" + matchplaySlugname + "' AND tournament = '" + tournId + "' );");

            if (Convert.ToInt32(new SqlCommand(sb2.ToString(), dbConn).ExecuteScalar()) > 0)
            {
                sb2.Clear();
                sb2.Append("UPDATE golf.dbo.leaderboardMatchPlay SET team1Points = '" + Team1points + "', team2Points = '" + Team2points + "', updated = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "' WHERE (year = " + DateTime.Now.Year + " AND tour = '" + matchplaySlugname + "' AND tournament = '" + tournId + "' );");
                new SqlCommand(sb2.ToString(), dbConn).ExecuteNonQuery();
            }
            else
            {
                sb2.Clear();
                sb2.Append("INSERT INTO golf.dbo.leaderboardMatchPlay (tour ,tournament ,year ,tournamentLogo ,team1 ,team2 ,team1ShortName ,team2ShortName ,team1Points ,team2Points ,team1Colour ,team2Colour ,team1Logo ,team2Logo ,updated) VALUES ('" + matchplaySlugname + "','" + tournId + "','" + DateTime.Now.Year + "','','" + Team1Name + "','" + Team2Name + "','" + Team1ShortName + "','" + Team2ShortName + "','" + Team1points + "','" + Team2points + "','','','','','" + DateTime.Now + "');");
                new SqlCommand(sb2.ToString(), dbConn).ExecuteNonQuery();
            }
        }

        private static string fixToPar(string score)
        {
            string retText = score;

            if (retText == "0")
            {
                retText = "E";
            }
            else if (retText.IndexOf("-") < 0 && retText != "E")
            {
                int tempInt = -1;
                bool result = Int32.TryParse(retText, out tempInt);
                if (result)
                {
                    retText = "+" + retText;
                }
            }

            return retText;
        }

        private string FixStatus(string status)
        {
            string configOptions = ConfigurationManager.AppSettings["statuses"];
            string tempStatus = status.ToLower();

            Hashtable statuses = new Hashtable();
            string[] options = configOptions.Split(';');
            foreach (string option in options)
            {
                string[] values = option.Split('|');
                statuses.Add(values[0].ToString(), values[1].ToString());
            }

            if (statuses.ContainsKey(tempStatus))
            {
                status = statuses[tempStatus].ToString();
            }

            return status;
        }
    }
}