﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace golfother
{
    public class runInfo
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public List<string> Errors { get; set; }
    }
}